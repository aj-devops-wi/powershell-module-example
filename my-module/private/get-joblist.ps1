function Get-MyJobList {
    param(
        [Parameter(Mandatory=$True)][ValidateNotNullOrEmpty()][string]$blockName,
        [Parameter(Mandatory=$False)][ValidateNotNullOrEmpty()][string]$jsonJobFilePath = "./prod-joblist.json"
    )

    $jobList= New-Object 'System.Collections.Generic.List[String]'

    try{
        $jobData = Get-Content -Path $jsonJobFilePath | ConvertFrom-Json
        #$jobData = Get-Content -Path "./prod-CIPRD.yml" | ConvertFrom-Yaml -Ordered -AllDocuments
        foreach($singleGroup in $jobData){
            if($singleGroup.groupname -eq $blockName){
                foreach($singlejob in $singleGroup.jobs){
                    $joblist += ($singlejob.jobname + "," + $singlejob.config)
                }
            }
        }
                
        Write-Host "========================================================================================"  
        return  $jobList
    }
    catch{
	    $msg = $Error[0].Exception.Message
	    Write-Host "############## Exited with $msg ##############`n"
    }
}
