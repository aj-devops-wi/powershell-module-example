function Get-JobOrder {
    param(
        [Parameter(Mandatory=$False)][ValidateNotNullOrEmpty()][string]$jsonJobFilePath = "./prod-myjobs.json"
    )

    try{
        $jobOrderList= New-Object System.Collections.ArrayList
        $jobData = Get-Content -Path $jsonJobFilePath | ConvertFrom-Json
        #$jobData = Get-Content -Path "./prod-CIPRD.yml" | ConvertFrom-Yaml -Ordered -AllDocuments
        foreach($singleGroup in $jobData){
            foreach($singlejob in $singleGroup.jobs){
                $jobOrderlist += ($singlejob.jobname + "," + $singlejob.config)
            }
        }
        return $jobOrderList
    }
    catch
    {
	    $msg = $Error[0].Exception.Message
	    Write-Host "############## Exited with $msg ##############`n"
    }
}
